<!DOCTYPE html>
<html>

<head>
    <title>FirePlay - Inicio</title>
    <meta charset="UTF-8">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <?php include('pages/startBodyLinks.php'); ?>
</head>

<body class="bg-dark text-light">

    <?php include('pages/navbar.php'); ?>

    <?php include('pages/carousel.php'); ?>

    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-4">
                <aside class="mdc-drawer bg-dark">
                    <div class="mdc-drawer__content">
                        <nav class="mdc-list">
                            <a class="mdc-list-item mdc-list-item--activated" href="#" aria-current="page">
                                <i class="material-icons mdc-list-item__graphic" style="color: deeppink;" aria-hidden="true">live_tv</i>
                                <span class="mdc-list-item__text text-light">Principal</span>
                            </a>
                            <a class="mdc-list-item" href="#">
                                <i class="material-icons mdc-list-item__graphic" style="color: deeppink;" aria-hidden="true">star</i>
                                <span class="mdc-list-item__text text-light">Favoritos</span>
                            </a>
                            <a class="mdc-list-item" href="#">
                                <i class="material-icons mdc-list-item__graphic" style="color: deeppink;" aria-hidden="true">share</i>
                                <span class="mdc-list-item__text text-light">Compartidos</span>
                            </a>

                            <hr class="mdc-list-divider" style="background-color:white;">
                            <h6 class="mdc-list-group__subheader text-white">Categorias:</h6>
                            <a class="mdc-list-item" href="#">
                                <i class="material-icons mdc-list-item__graphic" style="color: deeppink;" aria-hidden="true">motorcycle</i>
                                <span class="mdc-list-item__text text-light">Accion</span>
                            </a>
                            <a class="mdc-list-item" href="#">
                                <i class="material-icons mdc-list-item__graphic" style="color: deeppink;" aria-hidden="true">child_friendly</i>
                                <span class="mdc-list-item__text text-light">Infantiles</span>
                            </a>
                            <a class="mdc-list-item" href="#">
                                <i class="material-icons mdc-list-item__graphic" style="color: deeppink;" aria-hidden="true">directions_run</i>
                                <span class="mdc-list-item__text text-light">Terror</span>
                            </a>
                            <a class="mdc-list-item" href="#">
                                <i class="material-icons mdc-list-item__graphic" style="color: deeppink;" aria-hidden="true">tag_faces</i>
                                <span class="mdc-list-item__text text-light">Comedia</span>
                            </a>
                            <a class="mdc-list-item" href="#">
                                <i class="material-icons mdc-list-item__graphic" style="color: deeppink;" aria-hidden="true">wc</i>
                                <span class="mdc-list-item__text text-light">Drama</span>
                            </a>
                            <a class="mdc-list-item" href="#">
                                <i class="material-icons mdc-list-item__graphic" style="color: deeppink;" aria-hidden="true">spa</i>
                                <span class="mdc-list-item__text text-light">Cristianas</span>
                            </a>
                        </nav>
                    </div>
                </aside>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-8">
                <div class="alert alert-dark mt-4" role="alert">
                    <h3 class="text-dark">Películas Online</h3>
                    <p class="text-dark">
                        FirePlay.tk es un sitio ideal para ver películas y series online. Nuestro sistema se preocupa por tener lo último del cine en calidad full HD.
                    </p>
                    <p class="text-dark">
                        Para ver una película o serie de televisión puedes seguir uno de los enlaces de genero o año de estreno en la parte izquierda del sitio, luego el enlace te llevará al reproductor donde solo tienes que dar click en el boton de play. Te invitamos a compartir este genial sitio con tus amigos y familiares.
                    </p>
                </div>
                <h2 class="mt-4">Peliculas Actualizadas</h2>
                <!--Primer Row-->
                <div class="row d-flex align-items-center justify-content-center">
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/bladeRunner.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center"><a class="text-white" href="pages/informacion.php" style="text-decoration: none;">Blade Runner 2049</a></h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/capitanAmerica.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">Capitan América</h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/divergente.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">Divergente</h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/dumbo.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">Dumbo</h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/losCroods.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">Los Croods</h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/sabotaje.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">Sabotaje</h5>
                        </div>
                    </div>
                </div>
                <!--Segundo Row-->
                <div class="row d-flex align-items-center justify-content-center">
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/thor.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">Thor</h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/madagascar3.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">Madagascar 3</h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/thetourist.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">The Tourist</h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/toctoc.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">Toc Toc</h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/felizdiadetumuerte.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">Feliz dia de tu muerte</h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/astral.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">Astral</h5>
                        </div>
                    </div>
                </div>
                <!--Tercer Row-->
                <div class="row d-flex align-items-center justify-content-center">
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/spoor.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">Spoor</h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/tiempo.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">Tiempo</h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/madre.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">Madre</h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/lamontanaentrenosotros.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">La montaña entre nosotros</h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/trumbo.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">Trumbo</h5>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-2 mb-3">
                        <div class="card bg-dark" style="width: auto;">
                            <img src="images/portadasPeliculas/bienvenidosalnorte.jpg" class="card-img-top" alt="Portada Pelicula">
                            <h5 class="card-title text-light text-center">Bienvenidos al Norte</h5>
                        </div>
                    </div>
                </div>
                <div class="container-fluid d-flex align-items-center justify-content-center" style="background-color:#263238; height:3rem;">
                    <div class="text-center">
                        <a href="#" style="color:white; text-decoration: none;">Ver más</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('pages/footer.php'); ?>
    <?php include('pages/finalBodyScripts.php'); ?>
</body>

</html>