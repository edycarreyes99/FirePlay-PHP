<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FirePlay - La Llorona</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://file.myfontastic.com/f7iknQawT9jwoVWrcbt9aY/icons.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/index.scss">
</head>

<body class="bg-dark">
    <div class="container">
        <nav class="navbar navbar-dark bg-dark navbar-expand-lg">
            <a class="navbar-brand" href="../index.php">
                <img src="../images/logo/FirePlayDarkLogo.jpg" width="50" height="50">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../index.php">Inicio<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Series</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Estreno</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Categorias
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="#">Accion</a>
                            <a class="dropdown-item" href="#">Terror</a>
                            <a class="dropdown-item" href="#">Aventura</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a href="pages/login.php" class="mdc-icon-button material-icons text-light" style="text-decoration:none;">account_circle</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="row">
            <div class="col-sm-12 col-md-4 col-lg-4 mb-4">
                <div class="container-fluid d-flex justify-content-center align-items-center"><img class="ml-4" src="../images/portadasPeliculas/bladerunner.jpg" alt="Portada Imagen" height="300"></div>
                <div class="container-fluid d-flex justify-content-center align-items-center mt-3"><a target="blank" href="https://www.youtube.com/watch?v=BkBM0gL3s2M&feature=youtu.be" class="btn btn-outline-light" style="text-decoration: none;">Ver Trailer</a></div>
            </div>
            <div class="col-sm-12 col-md-8 col-lg-8">
                <h5 class="text-white" style="color: deeppink !important;">
                    <b>Blade Runner 2049</b>
                </h5>
                <div class="text-white">
                    Título Original: <strong class="text-warning">Blade Runner 2049</strong>
                </div>
                <div class="text-white">
                    Año: <strong class="text-warning">2017</strong>
                </div>
                <div class="text-white">
                    Géneros: <strong class="text-warning">Ciencia Ficción, Accion</strong>
                </div>
                <div class="text-white">
                    Sípnosis: <br>
                    <p class="text-warning">
                        En 2049, seres humanos creados a partir de bioingeniería llamados replicantes se han integrado en la sociedad, ya que la vida vinculada a la bioingeniería resultó ser necesaria para asegurar la supervivencia de la humanidad. K, un modelo más reciente creado para obedecer, funciona como un blade runner para el LAPD, cazando y "retirando" modelos de replicantes clandestinos más viejos. Su vida en el hogar la comparte con su pareja holográfica Joi, un producto de Wallace Corporation.
                    </p>
                </div>
                <div class="text-white">
                    Reparto: <strong class="text-warning">Ryan Gosling, Harison Ford</strong>
                </div>
            </div>
        </div>
        <div class="container mt-4" style="max-width: 40rem !important;">
            <div class="alert alert-warning" role="alert">
                Está película se encuentra en buena calidad, puedes seleccionar la calidad en el reproductor.
            </div>
        </div>
        <h4 class="text-light">Reproducción</h4>
        <div class="container-fluid">
            <iframe height="600" src="https://oload.fun/embed/ZkVj8oOokig" scrolling="no" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" oallowfullscreen="true" msallowfullscreen="true" width="100%" height="100%" frameborder="0"></iframe>
        </div>
        <a href="#" class="btn btn-outline-light ml-3 mt-4" style="text-decoration: none;">Descargar Película</a>
    </div>
    <footer class="footer bg-dark">
        <div class="social">
            <a target="blank" href="https://www.facebook.com/edycar.reyes99" class="icon-facebook"></a>
            <a target="blank" href="https://twitter.com/ReyesEdycar" class="icon-twitter"></a>
            <a target="blank" href="https://github.com/edycarreyes99" class="icon-github"></a>
            <a target="blank" href="https://github.com/Fire-Codes" class="icon-firecodes"></a>
        </div>
        <p class="copy">&copy; KinGames 2019 - Todos los derechos reservados FireCodes Edycar Reyes</p>
    </footer>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>