<nav class="navbar navbar-dark bg-dark navbar-expand-lg">
    <a class="navbar-brand" href="index.php">
        <img src="images/logo/FirePlayDarkLogo.jpg" width="50" height="50">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown"
        aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Inicio<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Series</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Estreno</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Categorias
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="#">Accion</a>
                    <a class="dropdown-item" href="#">Terror</a>
                    <a class="dropdown-item" href="#">Aventura</a>
                </div>
            </li>
            <li class="nav-item">
                <a href="pages/login.php" class="mdc-icon-button material-icons text-light"
                    style="text-decoration:none;">account_circle</a>
            </li>
        </ul>
    </div>
</nav>