<footer class="footer bg-dark">
    <div class="social">
        <a target="blank" href="https://www.facebook.com/edycar.reyes99" class="icon-facebook"></a>
        <a target="blank" href="https://twitter.com/ReyesEdycar" class="icon-twitter"></a>
        <a target="blank" href="https://github.com/edycarreyes99" class="icon-github"></a>
        <a target="blank" href="https://github.com/Fire-Codes" class="icon-firecodes"></a>
    </div>
    <p class="copy">&copy; KinGames 2019 - Todos los derechos reservados FireCodes Edycar Reyes</p>
</footer>