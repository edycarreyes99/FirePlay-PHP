<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FirePlay - Registro</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">
    <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link href="https://file.myfontastic.com/f7iknQawT9jwoVWrcbt9aY/icons.css" rel="stylesheet">
    <link rel="stylesheet" href="../css/index.scss">
    <script src="https://www.google.com/recaptcha/api.js?render=6Lf6j6IUAAAAADcP0usb8kI1YYnMoCj27OkVMIsc"></script>
    <script>
        grecaptcha.ready(function() {
            grecaptcha.execute('6Lf6j6IUAAAAADcP0usb8kI1YYnMoCj27OkVMIsc', {
                action: 'homepage'
            }).then(function(token) {
                consol.debug('El recaptcha ha sido cargado');
            });
        });
    </script>
</head>

<body class="bg-dark text-light">
    <nav class="navbar navbar-dark bg-dark navbar-expand-lg">
        <a class="navbar-brand" href="../index.php">
            <img src="../images/logo/FirePlayDarkLogo.jpg" width="50" height="50">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="../index.php">Inicio<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Series</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Estreno</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Categorias
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Accion</a>
                        <a class="dropdown-item" href="#">Terror</a>
                        <a class="dropdown-item" href="#">Aventura</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a href="pages/login.php" class="mdc-icon-button material-icons text-light" style="text-decoration:none;">account_circle</a>
                </li>
            </ul>
        </div>
    </nav>
    <div class="container d-flex align-items-center justify-content-center" style="min-height: 80vh;">
        <row>
            <div class="col-sm-12 col-md-12 col-lg-12">
                <img src="../images/login/avatar.png" width="250">
            </div>
        </row>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <form action="../php/registrar.php" method="POST">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" name="usuario" placeholder="Ingrese el Username">
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="username">Nombres</label>
                                <input type="text" class="form-control" name="nombre" placeholder="Ingrese el Nombre de Usuario">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="username">Apellidos</label>
                                <input type="text" class="form-control" name="apellidos" placeholder="Ingrese el Nombre de Usuario">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Correo Electrónico</label>
                        <input type="email" class="form-control" name="correo" placeholder="Ingrese el E-Mail">
                        <small id="emailAyuda" class="form-text text-muted">¡Nunca compartiremos tu email con
                            nadie!.</small>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Contraseña</label>
                        <input type="password" class="form-control" placeholder="Ingrese la Contraseña" name="clave">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Telefono</label>
                        <input type="number" class="form-control" placeholder="Ingrese su Telefono" name="telefono">
                    </div>
                    <div class="g-recaptcha" data-sitekey="6Lf6j6IUAAAAAMvkqLgwqNdHDXIi65QOkxyT4vVF"></div>
                    <div class="container d-flex justify-content-center align-items-center">
                        <button type="submit" class="btn btn-primary">Registrarse</button>
                    </div>
                    <br>
                    <a href="login.php" class="mt-4">¿Ya tienes una cuenta? Ingresa aquí.</a>
                </form>
            </div>
        </div>
    </div>
    <footer class="footer bg-dark">
        <div class="social">
            <a target="blank" href="https://www.facebook.com/edycar.reyes99" class="icon-facebook"></a>
            <a target="blank" href="https://twitter.com/ReyesEdycar" class="icon-twitter"></a>
            <a target="blank" href="https://github.com/edycarreyes99" class="icon-github"></a>
            <a target="blank" href="https://github.com/Fire-Codes" class="icon-firecodes"></a>
        </div>
        <p class="copy">&copy; KinGames 2019 - Todos los derechos reservados FireCodes Edycar Reyes</p>
    </footer>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>