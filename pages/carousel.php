<div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
    <ol class="carousel-indicators mt-n5">
        <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="images/banner/4.jpg" class="d-block w-100" alt="Feliz dia de tu muerte 2">
            <div class="carousel-caption d-none d-md-block" style="margin-bottom: 8rem !important;">
                <h5>Feliz Dia de tu Muerte 2</h5>

            </div>
        </div>
        <div class="carousel-item">
            <img src="images/banner/1.jpg" class="d-block w-100" alt="La Llorona">
            <div class="carousel-caption d-none d-md-block" style="margin-bottom: 8rem !important;">
                <h5>La Llorona.</h5>

            </div>
        </div>
        <div class="carousel-item">
            <img src="images/banner/2.jpeg" class="d-block w-100" alt="Shazam">
            <div class="carousel-caption d-none d-md-block" style="margin-bottom: 8rem !important;">
                <h5>Shazam</h5>

            </div>
        </div>

        <div class="carousel-item">
            <img src="images/banner/3.jpg" class="d-block w-100" alt="Cementerio Maldito">
            <div class="carousel-caption d-none d-md-block" style="margin-bottom: 8rem !important;">
                <h5>Cementerio maldito.</h5>

            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>